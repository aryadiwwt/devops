#!/usr/bin/env bash

set -e
set -o pipefail

if [ $# -eq 0 ]
  then
    echo "Usage: deploy.sh [version] [namespace] [service]"
    exit 1
fi

sed -E -i.bak.$(date +"%Y%m%d%H%M%S") "s/(.$2\/$3:).*/\1$1/" docker-compose.yml
docker-compose up -d
